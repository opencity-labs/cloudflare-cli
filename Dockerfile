# This file is a template, and might need editing before it works on your project.
FROM louisbl/php:5.4-cli

WORKDIR /srv

# Customize any core extensions here
RUN apt-get update && apt-get install -y \
    libmcrypt-dev \
    && docker-php-ext-install -j$(nproc) mcrypt

#COPY config/php.ini /usr/local/etc/php/

COPY composer.json /srv/

ENV COMPOSER_ALLOW_SUPERUSER=1
RUN echo "Running composer"  \
	&& composer install --prefer-dist --no-scripts --no-dev \
	&& composer clearcache
